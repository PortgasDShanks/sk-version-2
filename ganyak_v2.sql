-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2021 at 11:21 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ganyak`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '0=pending, 1=accepted'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `applications`
--

INSERT INTO `applications` (`id`, `user_id`, `service_id`, `program_id`, `note`, `status`) VALUES
(9, 7, 1, 2, '', 0),
(8, 7, 1, 1, '', 2),
(10, 7, 2, 3, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `chat_content` text NOT NULL,
  `chat_datetime` datetime NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `sender_id`, `receiver_id`, `chat_content`, `chat_datetime`, `status`) VALUES
(1, 1, 4, 'asdasdasdasdasdas', '2021-09-13 15:30:11', 0),
(2, 4, 1, 'test', '2021-09-13 15:30:36', 0),
(3, 1, 5, 'asdasdasdasdasdas', '2021-09-13 15:30:11', 0),
(5, 1, 4, 'asdasdasdasdas', '2021-09-13 23:23:51', 0),
(6, 1, 4, 'asdasdasdasqweqweqw', '2021-09-13 23:24:43', 0),
(7, 1, 4, 'asqweqweasdaczxcasqeqweqwdased qdws dasdas', '2021-09-13 23:24:51', 0),
(8, 1, 4, 'qweqwtqrqwreqweqweqw', '2021-09-13 23:25:21', 0),
(9, 4, 1, 'test', '2021-09-13 15:30:36', 0),
(10, 5, 1, 'test', '2021-09-13 15:30:36', 0),
(11, 1, 4, 'asdasdas', '2021-09-14 00:51:01', 0),
(12, 1, 4, 'asdasdasdas', '2021-09-14 00:51:26', 0),
(13, 1, 4, 'test', '2021-09-14 00:53:00', 0),
(14, 1, 4, 'qweqwr', '2021-09-14 00:53:41', 0),
(15, 1, 4, 'test', '2021-09-14 00:54:06', 0),
(16, 1, 4, 'yuttyutyuty', '2021-09-14 00:54:46', 0),
(17, 1, 4, 'asdasdasqweqweqw', '2021-09-14 00:57:32', 0),
(18, 1, 4, 'qqqq', '2021-09-14 01:02:36', 0),
(19, 1, 4, 'adwqaeqweqw', '2021-09-14 01:05:35', 0),
(20, 1, 4, 'qweqweqwe', '2021-09-14 01:05:41', 0),
(21, 1, 4, 'asdasdasd', '2021-09-14 01:06:40', 0),
(22, 1, 4, 'wqeqweasdasdasd', '2021-09-14 01:07:53', 0),
(23, 1, 4, 'qweqwxzczxczxc', '2021-09-14 01:08:22', 0),
(24, 1, 4, 'asdasdasdas', '2021-09-14 01:11:29', 0),
(25, 1, 4, 'asdasdasdas', '2021-09-14 01:12:10', 0),
(26, 1, 4, 'asdasdasdas', '2021-09-14 01:13:16', 0),
(27, 1, 4, 'asdasdas', '2021-09-14 01:14:09', 0),
(28, 1, 4, 'sdsdsds', '2021-09-14 01:17:37', 0),
(29, 1, 0, 'asdasdas', '2021-09-14 02:10:08', 0),
(30, 1, 4, 'asdasdasdasd', '2021-09-14 02:10:13', 0),
(31, 1, 4, 'tqqqyyfff', '2021-09-14 02:13:14', 0),
(32, 1, 4, 'qweqweqweqw', '2021-09-14 02:13:56', 0),
(33, 1, 4, 'asdasd', '2021-09-14 02:15:37', 0);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `event_name` text NOT NULL,
  `event_description` text NOT NULL,
  `program_id` int(11) NOT NULL,
  `event_date_from` datetime NOT NULL,
  `event_date_to` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_name`, `event_description`, `program_id`, `event_date_from`, `event_date_to`, `created_by`) VALUES
(1, 'Test event', 'ertwerdfsd fsd fsd fsdfsd', 1, '2021-11-10 00:00:00', '2021-11-12 00:00:00', 1),
(2, 'tedsaas das das das dasd asd asdas', 'werwer234234 23423 423', 2, '2021-11-15 00:00:00', '2021-11-17 00:00:00', 1),
(3, 'highlight current date fullcalendar', 'highlight current date fullcalendar', 3, '2021-11-16 00:00:00', '2021-11-18 00:00:00', 1),
(4, 'Moves the calendar to the current date.', 'Moves the calendar to the current date.', 1, '2021-11-17 00:00:00', '2021-11-18 00:00:00', 1),
(5, 'Whether or not to display a marker indicating the current time.', 'Whether or not to display a marker indicating the current time.', 2, '2021-11-22 00:00:00', '2021-11-23 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `migration` text NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '20210408051901_create_password_resets_table', 1),
(2, '20210408051901_create_roles_table', 1),
(3, '20210408051901_create_users_table', 1),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(7, '2016_06_01_000004_create_oauth_clients_table', 2),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(9, '2019_08_19_000000_create_failed_jobs_table', 2),
(10, '2021_09_20_200753_create_services_table', 2),
(11, '2021_09_22_102113_add_bday_to_users_table', 3),
(12, '2021_09_23_205518_create_users_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `program_name` varchar(255) NOT NULL,
  `program_desc` text NOT NULL,
  `appliable_or_not` int(1) NOT NULL DEFAULT 1,
  `status` int(1) NOT NULL COMMENT '0=Ongoing, 1=Finished, 2=Cancelled',
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `service_id`, `program_name`, `program_desc`, `appliable_or_not`, `status`, `added_by`) VALUES
(1, 1, 'Feeding Program', 'KAdto kamu d', 1, 0, 1),
(2, 1, 'Program 2', 'Sample program', 1, 0, 1),
(3, 2, 'Program test', 'Program test', 1, 0, 1),
(4, 2, 'Program sample', 'Program sample', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `role` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `service_desc` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `member_id`, `service_name`, `service_desc`) VALUES
(1, 4, 'service 1', 'asdasdas'),
(2, 4, 'service 2', 'test'),
(3, 6, 'service 3', 'testsadfasdas'),
(4, 4, 'service 4', 'erwerwerwe'),
(5, 4, 'service 5', 'dasd');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL DEFAULT '',
  `fullname` varchar(200) DEFAULT NULL,
  `contactNo` varchar(20) NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `password` text NOT NULL,
  `role_id` varchar(2) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `dob` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `fullname`, `contactNo`, `username`, `password`, `role_id`, `remember_token`, `updated_at`, `created_at`, `dob`) VALUES
(1, 'ibayonabel@gmail.com', 'Abel', '', 'abel', '$2y$10$MzO2PrEN5UjsIVzq2fSX/uM3lklTyuB0NDGhTxNMgP67i9z9cGOiu', 'C', NULL, '2021-08-17 23:57:53', '2021-08-17 23:57:53', NULL),
(4, 'dams2020@gmail.com', 'Jihyo Park', '09213807764', 'jihyo', '$2y$10$nmtUs9k4/itdIt75fX/uoekuwsNuphhFMjtr2RwTcaaESv3VEhrDy', 'M', NULL, NULL, '2021-09-08 06:31:06', NULL),
(5, 'test@gmail.com', 'Chaeyoung Son', '09772645142', 'chaeyoung', '$2y$10$vMDMfuL/rthLeCtGJA2M4.m82rGe8V5AjuFsQgSLRqqINrJYKKIdS', 'M', NULL, NULL, '2021-09-08 06:45:29', NULL),
(6, 'dams2020@gmail.com', 'Tzuyu Chou', '09213807765', 'tzuyu', '$2y$10$cz1zgPEwIXYzurAdYONZb.NSOUMm9jYuEG.la3B8958rsk.lrNEUC', 'M', NULL, NULL, '2021-09-08 06:45:52', NULL),
(7, 'ibayonabel@gmail.com', 'Jochelle bravo1', '09454564469', 'bjochelle@gmail.com', '$2y$10$dPtr1x6vcfl9mzcpoajrAeHYxgh7uTNJ.z3qMrcJbBF73b91jgwjW', 'U', NULL, '2021-11-10 21:55:00', '2021-09-09 14:22:34', '2011-09-22'),
(8, 'bjochel22le@gmail.com', 'dsadasda', '09107505919', 'bjochel22le@gmail.com', '$2y$10$QCAwKznZI3ORHswovu6LSeCsvyZt5CWhaDrlrg2CpqnnnoT4sP5Eq', 'U', NULL, '2021-09-09 14:25:05', '2021-09-09 14:25:05', NULL),
(9, 'bjochelle11@gmail.com', 'sadasd', '09107505919', 'bjochelle11@gmail.com', '$2y$10$WZpLO798vZ2Lx6wJgaX42u/ebDs1XDMd0rJ8rbwCZKgk5bpm17vUe', 'U', NULL, '2021-09-09 19:49:47', '2021-09-09 19:49:47', NULL),
(10, 'q@gnail.com', 'dsadas', '31231232313', 'q@gnail.com', '$2y$10$wV80pLyj7Iinr2p3V78pVei25rpGzdFTaW4erSB3oC8Vrwz9..7v.', 'U', NULL, '2021-09-22 19:28:07', '2021-09-22 19:28:07', '2021-09-22'),
(11, 'a@gmail.com', 'liammm', '31231232131', 'a@gmail.com', '$2y$10$GVZS24g0kXEG5xJ1y4Pya.la9ekiyOt0iKFxze7fwWD6TwLAtFehG', 'U', NULL, '2021-09-23 22:38:25', '2021-09-23 22:38:25', '2021-09-27'),
(12, 'abel@gmail.com', 'abels', '09764649498', 'abel@gmail.com', '$2y$10$KAcJcgZS5gj4EV8olUjYbejLpkB9aaipJ9uDS6UbLfIYhXFLa1s8G', 'U', NULL, '2021-09-24 06:47:45', '2021-09-23 23:23:14', '2021-09-23'),
(13, 'email@gmail.com', 'jochele', '09107525454', 'email@gmail.com', '$2y$10$N0iv6iTixr6G6jg5PsfNEuRmLPQtKV/yOZ8EXA4m8AxfkXLh0qsQK', 'U', NULL, '2021-11-10 21:46:18', '2021-11-10 21:46:18', '2021-11-12'),
(14, 'sadas@gmail.com', 'dsad', '09878787878', 'sadas@gmail.com', '$2y$10$L3OtefXjUXDSWdCj23XAq.9A4.MBSW6B7EFASIVWl.yjldZGrToxq', 'U', NULL, '2021-11-10 21:47:43', '2021-11-10 21:47:43', '2021-11-26');

-- --------------------------------------------------------

--
-- Table structure for table `user_uploads`
--

CREATE TABLE `user_uploads` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `filetype` text NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filesize` text NOT NULL,
  `iconsize` text NOT NULL,
  `file_category` varchar(10) NOT NULL,
  `announcement_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_uploads`
--

INSERT INTO `user_uploads` (`id`, `user_id`, `slug`, `filetype`, `filename`, `filesize`, `iconsize`, `file_category`, `announcement_id`, `created_at`) VALUES
(2, 1, 'public/assets/uploads/U81W20211002052013.png', 'PNG', 'logo_c', '0.037322', '100%', 'M', 0, '2021-09-29 08:20:01'),
(3, 1, 'public/assets/uploads/U81W20211002052013.png', 'PNG', 'logo_c', '0.037322', '100%', 'M', 0, '2021-09-29 08:20:08'),
(4, 1, 'public/assets/uploads/U81W20211002052013.png', 'PNG', 'logo_c', '0.037322', '100%', 'M', 0, '2021-09-29 09:02:57'),
(7, 1, 'public/assets/uploads/U81W20211002052013.png', 'PNG', 'logo_c', '0.037322', '100%', 'R', 0, '2021-09-29 13:10:50'),
(8, 4, 'public/assets/uploads/EBRU20210930061744.xlsx', 'XLSX', 'Book1', '0.015527', '25%', 'M', 0, '2021-09-29 14:17:44'),
(9, 4, 'public/assets/uploads/WXYK20210930062542.xlsx', 'XLSX', 'Book1', '0.015527', '25%', 'R', 0, '2021-09-29 14:25:42'),
(13, 1, 'public/assets/uploads/U81W20211002052013.png', 'PNG', 'logo_c', '0.037322', '100%', 'M', 0, '2021-10-01 09:38:49'),
(14, 1, 'public/assets/uploads/U81W20211002052013.png', 'PNG', 'logo_c', '0.037322', '100%', 'M', 0, '2021-10-01 09:39:01'),
(15, 1, 'public/assets/uploads/3LPL20211002052646.jpg', 'JPG', 'team_5', '0.166436', '100%', 'P', 0, '2021-10-01 13:08:19'),
(18, 1, 'public/assets/uploads/NUZR20211005021652.jpg', 'JPG', 'vue', '0.004188', '100%', 'N', 0, '2021-10-04 10:16:52'),
(19, 1, 'public/assets/uploads/H9IP20211005040325.jpg', 'JPG', '5', '0.021767', '100%', 'N', 30, '2021-10-04 12:03:25'),
(20, 1, 'public/assets/uploads/9CIT20211005041144.jpg', 'JPG', '2', '0.010613', '100%', 'N', 30, '2021-10-04 12:11:44'),
(21, 1, 'public/assets/uploads/RISJ20211005042601.PNG', 'PNG', 'erdAlegria', '0.042128', '100%', 'N', 0, '2021-10-04 12:26:01'),
(22, 1, 'public/assets/uploads/3IH220211005042636.jpg', 'JPG', 'bg_starbucks', '0.06203', '100%', 'N', 0, '2021-10-04 12:26:36'),
(23, 1, 'public/assets/uploads/ZY2Z20211007062412.docx', 'DOCX', 'Acknowledgement', '0.053632', '25%', 'N', 0, '2021-10-06 14:24:12'),
(24, 1, 'public/assets/uploads/WPMR20211007063052.docx', 'DOCX', 'GRAMMARIAN_CERTIFICATE', '0.053164', '25%', 'N', 0, '2021-10-06 14:30:52'),
(25, 1, 'public/assets/uploads/WPR020211007063233.docx', 'DOCX', 'APPENDIX_D_(CURRICULUM_VITAE)', '0.496867', '25%', 'N', 80, '2021-10-06 14:32:33'),
(26, 1, 'public/assets/uploads/FTGP20211007065157.jpg', 'JPG', 'image_2', '0.08094', '100%', 'N', 82, '2021-10-06 14:51:57'),
(0, 1, 'public/assets/uploads/R6NQ20211111070208.docx', 'DOCX', 'Chatbot', '0.020326', '25%', 'N', 0, '2021-11-10 23:02:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`email`) USING BTREE;

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applications`
--
ALTER TABLE `applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
