-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2021 at 12:33 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ganyak`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `user_id`, `content`, `datetime`) VALUES
(1, 1, '0', '2021-09-22 20:46:42'),
(2, 1, 'sdasdasdqweqwe qweqweqweqw', '2021-09-22 20:51:36'),
(3, 1, 'test', '2021-09-22 20:54:26'),
(4, 1, 'sadasdasdas', '2021-09-22 20:55:49'),
(5, 1, 'sample 23', '2021-09-22 20:56:59'),
(6, 4, 'may meeting kita mag 12am', '2021-09-22 20:57:59'),
(7, 4, 'test', '2021-09-22 20:59:33'),
(8, 1, 'asdasdasdasdasdasdasd asdasdasdasd', '2021-09-22 22:46:58');

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '0=pending, 1=accepted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `chat_content` text NOT NULL,
  `chat_datetime` datetime NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `sender_id`, `receiver_id`, `chat_content`, `chat_datetime`, `status`) VALUES
(1, 1, 4, 'asdasdasdasdasdas', '2021-09-13 15:30:11', 0),
(2, 4, 1, 'test', '2021-09-13 15:30:36', 0),
(3, 1, 5, 'asdasdasdasdasdas', '2021-09-13 15:30:11', 0),
(5, 1, 4, 'asdasdasdasdas', '2021-09-13 23:23:51', 0),
(6, 1, 4, 'asdasdasdasqweqweqw', '2021-09-13 23:24:43', 0),
(7, 1, 4, 'asqweqweasdaczxcasqeqweqwdased qdws dasdas', '2021-09-13 23:24:51', 0),
(8, 1, 4, 'qweqwtqrqwreqweqweqw', '2021-09-13 23:25:21', 0),
(9, 4, 1, 'test', '2021-09-13 15:30:36', 0),
(10, 5, 1, 'test', '2021-09-13 15:30:36', 0),
(11, 1, 4, 'asdasdas', '2021-09-14 00:51:01', 0),
(12, 1, 4, 'asdasdasdas', '2021-09-14 00:51:26', 0),
(13, 1, 4, 'test', '2021-09-14 00:53:00', 0),
(14, 1, 4, 'qweqwr', '2021-09-14 00:53:41', 0),
(15, 1, 4, 'test', '2021-09-14 00:54:06', 0),
(16, 1, 4, 'yuttyutyuty', '2021-09-14 00:54:46', 0),
(17, 1, 4, 'asdasdasqweqweqw', '2021-09-14 00:57:32', 0),
(18, 1, 4, 'qqqq', '2021-09-14 01:02:36', 0),
(19, 1, 4, 'adwqaeqweqw', '2021-09-14 01:05:35', 0),
(20, 1, 4, 'qweqweqwe', '2021-09-14 01:05:41', 0),
(21, 1, 4, 'asdasdasd', '2021-09-14 01:06:40', 0),
(22, 1, 4, 'wqeqweasdasdasd', '2021-09-14 01:07:53', 0),
(23, 1, 4, 'qweqwxzczxczxc', '2021-09-14 01:08:22', 0),
(24, 1, 4, 'asdasdasdas', '2021-09-14 01:11:29', 0),
(25, 1, 4, 'asdasdasdas', '2021-09-14 01:12:10', 0),
(26, 1, 4, 'asdasdasdas', '2021-09-14 01:13:16', 0),
(27, 1, 4, 'asdasdas', '2021-09-14 01:14:09', 0),
(28, 1, 4, 'sdsdsds', '2021-09-14 01:17:37', 0),
(29, 1, 0, 'asdasdas', '2021-09-14 02:10:08', 0),
(30, 1, 4, 'asdasdasdasd', '2021-09-14 02:10:13', 0),
(31, 1, 4, 'tqqqyyfff', '2021-09-14 02:13:14', 0),
(32, 1, 4, 'qweqweqweqw', '2021-09-14 02:13:56', 0),
(33, 1, 4, 'asdasd', '2021-09-14 02:15:37', 0);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `event_name` text NOT NULL,
  `event_datetime` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `migrations` text NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migrations`, `batch`) VALUES
(1, '20210408051901_create_password_resets_table', 1),
(2, '20210408051901_create_roles_table', 1),
(3, '20210408051901_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `program_name` varchar(255) NOT NULL,
  `program_desc` text NOT NULL,
  `appliable_or_not` int(1) NOT NULL DEFAULT 1,
  `status` int(1) NOT NULL COMMENT '0=Ongoing, 1=Finished, 2=Cancelled',
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `service_id`, `program_name`, `program_desc`, `appliable_or_not`, `status`, `added_by`) VALUES
(1, 1, 'Feeding Program', 'KAdto kamu d', 1, 0, 1),
(2, 1, 'Program 2', 'Sample program', 1, 0, 1),
(3, 2, 'Program test', 'Program test', 1, 0, 1),
(4, 2, 'Program sample', 'Program sample', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `role` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `service_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `member_id`, `service_name`, `service_desc`) VALUES
(1, 4, 'service 1', 'test'),
(2, 4, 'service 2', 'test'),
(3, 6, 'Service 3', 'testsadfasdas'),
(4, 4, 'service 4', 'erwerwerwe'),
(5, 6, 'service 5', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL DEFAULT '',
  `fullname` varchar(200) DEFAULT NULL,
  `contact_no` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `password` text NOT NULL,
  `role_id` varchar(2) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `login_status` int(1) NOT NULL COMMENT '0=logged-out, 1=loggedin, 2=Away'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `fullname`, `contact_no`, `address`, `username`, `password`, `role_id`, `remember_token`, `updated_at`, `created_at`, `login_status`) VALUES
(1, 'ibayonabel@gmail.com', 'Abel Bayon', '', '', 'abel', '$2y$10$MzO2PrEN5UjsIVzq2fSX/uM3lklTyuB0NDGhTxNMgP67i9z9cGOiu', 'C', NULL, '2021-08-17 23:57:53', '2021-08-17 23:57:53', 1),
(4, 'dams2020@gmail.com', 'Jihyo Park', '09213807764', '', 'jihyo', '$2y$10$nmtUs9k4/itdIt75fX/uoekuwsNuphhFMjtr2RwTcaaESv3VEhrDy', 'M', NULL, NULL, '2021-09-08 06:31:06', 2),
(5, 'test@gmail.com', 'Chaeyoung Son', '09772645142', '', 'chaeyoung', '$2y$10$vMDMfuL/rthLeCtGJA2M4.m82rGe8V5AjuFsQgSLRqqINrJYKKIdS', 'M', NULL, NULL, '2021-09-08 06:45:29', 0),
(6, 'dams2020@gmail.com', 'Tzuyu Chou', '09213807765', '', 'tzuyu', '$2y$10$cz1zgPEwIXYzurAdYONZb.NSOUMm9jYuEG.la3B8958rsk.lrNEUC', 'M', NULL, NULL, '2021-09-08 06:45:52', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`email`) USING BTREE;

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `applications`
--
ALTER TABLE `applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
