<?php

namespace App\Controllers;

use App\Core\App;
use App\Core\Auth;
use App\Core\Request;

class ProfileController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Profile";
        $user_id = Auth::user('id');
        $user_data = DB()->select("*", 'users', "id='$user_id'")->get();
        $user_avatar = DB()->select("*","user_uploads","user_id = '$user_id' AND file_category = 'P'")->get();

        return view('/auth/profile', compact('user_data', 'pageTitle', 'user_avatar'));
    }

    public function update()
    {
        $request = Request::validate('/profile', [
            'email' => ['required', 'email']
        ]);

        $user_id = Auth::user('id');

        $update_data = [
            'email' => "$request[email]",
            'fullname' => "$request[name]",
            'bio' => "$request[bio]"
        ];

        DB()->update('users', $update_data, "id = '$user_id'");
        redirect("/profile", ["message" => "Profile information updated.", "status" => 'success']);
    }

    public function changePass()
    {
        $request = Request::validate('/profile', [
            'old-password' => ['required'],
            'new-password' => ['required'],
            'confirm-password' => ['required']
        ]);

        $response_message = Auth::resetPassword($request);
        redirect("/profile", $response_message);
    }

    public function destroy($user_id)
    {
        Request::validate();
        DB()->delete('users', "id = '$user_id'");

        Auth::logout();
    }

    public function storeFile()
    {
        $user_id = Auth::user('id');
        if(Request::hasFile('upload_file')){
            $file_tmp = $_FILES['upload_file']['tmp_name'];
            $file_name = explode('_', str_replace(array('.', ' ', ',', '-'), '_', $_FILES['upload_file']['name']));
            array_pop($file_name);

            $fileSize = $_FILES['upload_file']['size'] / 1000000;
            $file_type = explode('.', $_FILES['upload_file']['name']);
            $file_type_end = end($file_type);
            $fileType = strtoupper($file_type_end);

            $filename = strtoupper(randChar(4)) . date('Ymdhis') . "." . $file_type_end;

            $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
            if (in_array($fileType, $image_icons_array)) {
                $previewSize = '100%';
            } else {
                $previewSize = '25%';
            }

            $folder = uniqid() . '-' . date('Ymdhis');
            $temp_dir = "public/assets/uploads/";

            $folderName = implode('_', $file_name);

            Request::storeAs($file_tmp, $temp_dir, $_FILES['upload_file']['type'], $filename);
            $this->saveFilesInDb($user_id, $temp_dir . $filename, $folderName, $fileSize, $fileType, $previewSize);
            echo $folder;
        }
    } 

    public function saveFilesInDb($user_id, $path, $file_name, $fileSize, $fileType, $previewSize)
    {
        $checker = DB()->select("count(*) as c", "user_uploads", "user_id = '$user_id' AND file_category = 'P'")->get();
        if($checker['c'] > 0){
            $update_data = [
                "slug" => $path,
                "filetype" => $fileType,
                "filename" => $file_name,
                "filesize" => $fileSize,
                "iconsize" => $previewSize
            ];
    
            DB()->update('user_uploads', $update_data, "user_id = '$user_id' AND file_category = 'P'");
        }else{
            $data_form = [
                "user_id" => $user_id,
                "slug" => $path,
                "filetype" => $fileType,
                "filename" => $file_name,
                "filesize" => $fileSize,
                "iconsize" => $previewSize,
                "file_category" => "P"
            ];
    
            DB()->insert("user_uploads", $data_form);
        }
        
    }
}
