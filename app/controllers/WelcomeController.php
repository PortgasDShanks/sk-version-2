<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;


class WelcomeController
{
    protected $pageTitle;

    public function home()
    {
        $pageTitle = "Home";
        $announcements = DB()->selectLoop("a.id, a.content, a.datetime, u.fullname, u.role_id, a.user_id", "announcements as a, users as u", "a.user_id = u.id  ORDER BY datetime DESC")->get();
        $user_id = Auth::user('id');
        $user_avatar = DB()->select("*","user_uploads","user_id = '$user_id' AND file_category = 'P'")->get();

        return view('/home', compact('pageTitle', 'announcements', 'user_avatar', 'user_id'));
    }

    public function store()
    {
        $request = Request::validate();

        $user = Auth::user('id');

        $dataToInsert = [
            "user_id" => $user,
            "content" => $request['message']
        ];

        $response = DB()->insert("announcements", $dataToInsert, "Y");

        echo $response;
    }

    public function delete()
    {
        $request = Request::validate();

        $announcement = DB()->delete("announcements", "id = '$request[aID]'");

        $file = DB()->delete("user_uploads", "id = '$request[fileID]'");

        echo $announcement;
    }

    public function uploadPostFile($id)
    {
        $user_id = Auth::user('id');
        if(Request::hasFile('upload_file')){
            $file_tmp = $_FILES['upload_file']['tmp_name'];
            $file_name = explode('_', str_replace(array('.', ' ', ',', '-'), '_', $_FILES['upload_file']['name']));
            array_pop($file_name);

            $fileSize = $_FILES['upload_file']['size'] / 1000000;
            $file_type = explode('.', $_FILES['upload_file']['name']);
            $file_type_end = end($file_type);
            $fileType = strtoupper($file_type_end);

            $filename = strtoupper(randChar(4)) . date('Ymdhis') . "." . $file_type_end;

            $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
            if (in_array($fileType, $image_icons_array)) {
                $previewSize = '100%';
            } else {
                $previewSize = '25%';
            }

            $folder = uniqid() . '-' . date('Ymdhis');
            $temp_dir = "public/assets/uploads/";

            $folderName = implode('_', $file_name);

            Request::storeAs($file_tmp, $temp_dir, $_FILES['upload_file']['type'], $filename);
            $save = $this->saveFilesInDb($user_id, $temp_dir . $filename, $folderName, $fileSize, $fileType, $previewSize, $id);
           
            
            echo $id;
        }
    }  

    public function saveFilesInDb($user_id, $path, $file_name, $fileSize, $fileType, $previewSize, $id)
    {
      

        $data_form = [
            "user_id" => $user_id,
            "slug" => $path,
            "filetype" => $fileType,
            "filename" => $file_name,
            "filesize" => $fileSize,
            "iconsize" => $previewSize,
            "file_category" => "N",
            "announcement_id" => $id
        ];

        DB()->insert("user_uploads", $data_form);
    }
}
