<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;


class ServicesController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Services";
        $users = DB()->selectLoop("*","users","role_id = 'M'")->get();
        $session = Auth::user('id');
        $where = (getRole(Auth::user('role_id')) == 'C')?"":"id = '$session'";
        $service_list = DB()->selectLoop("*","services", $where)->get();

        return view('/services/index', compact('pageTitle', 'users', 'service_list'));
    }

    public function programList($id)
    {
        $pageTitle = "Programs";
        $programs = DB()->selectLoop("p.program_name, s.service_name, p.program_desc, p.appliable_or_not, p.status, p.id as program_id, p.date_to","programs as p, services as s","p.service_id = s.id AND s.id = '$id'")->get();

        return view('/services/programs', compact('pageTitle','id', 'programs'));
    }

    public function applicants($id)
    {
        $pageTitle = "Applicants";
        $applicants = DB()->selectLoop("u.fullname, u.email, a.status, u.contactNo, u.id as user_id, a.id as application_id, a.program_id, a.date_applied","users as u, applications as a","u.id = a.user_id AND a.program_id = '$id'")->get();

        $program = DB()->select("*","programs","id = '$id'")->get();

        return view('/services/applicants', compact('pageTitle', 'id', 'applicants', 'program'));
    }

    public function storeProgram()
    {
        $request = Request::validate('', [
            "program_name" => ['required'],
            "program_desc" => ['required'],
            "program_date_from" => ['required'],
            "program_date_to" => ['required']
        ]);

        $appliable = (isset($request['appliable_check']))?1:0;

        $dataToInsert = [
            "service_id" => $request['service_id'],
            "program_name" => $request['program_name'],
            "program_desc" => $request['program_desc'],
            "date_from" => $request['program_date_from'],
            "date_to" => $request['program_date_to'],
            "appliable_or_not" => $appliable,
            "status" => 0,
            "added_by" => Auth::user('id')
        ];

        $response = DB()->insert("programs", $dataToInsert);

        echo $response;
    }

    public function store()
    {
        $request = Request::validate('', [
            "service_name" => ['required'],
            "service_desc" => ['required'],
            "assignedMember" => ['required']
        ]);

        $dataToInsert = [
            "member_id" => $request['assignedMember'],
            "service_name" => $request['service_name'],
            "service_desc" => $request['service_desc'],
        ];

        $response = DB()->insert("services", $dataToInsert);

        echo $response;
    }

    public function updateProgram()
    {
        $request = Request::validate();
        $appliable = (isset($request['appliable_check_update']))?1:0;
      

        $response = DB()->update("programs", ["program_name" => "$request[program_name_update]", "program_desc" => "$request[program_desc_update]", "date_from" => "$request[program_date_from_update]", "date_to" => "$request[program_date_to_update]", "appliable_or_not" => $appliable], "id = '$request[program_id]'");

        echo $response;
    }

    public function getProgramDetails()
    {
        $request = Request::validate();
        $getDetails = DB()->select("*","programs","id = '$request[id]'")->get();

        $a = array();
        $a['id'] = $getDetails['id'];
        $a['name'] = $getDetails['program_name'];
        $a['desc'] = $getDetails['program_desc'];
        $a['date_from'] = $getDetails['date_from'];
        $a['date_to'] = $getDetails['date_to'];
        $a['appliable'] = $getDetails['appliable_or_not'];

        
        echo json_encode($a);


    }

    public function cancel()
    {
        $request = Request::validate();

        $response = DB()->update("applications", ["status" => 2], "id = '$request[application]'");

        $applicant = DB()->select("*", "applications as a, users as u, programs as p", "a.user_id = u.id AND a.id = '$request[application]' AND a.program_id = p.id")->get();


        $msg_content = "We're sorry to tell that your application on ".$applicant['program_name']." was cancelled by the management. For more details please go to the SK office. Thank you";


       $email = $this->sendEmail($applicant['email'], $applicant['fullname'], $msg_content);

        echo $email;
    }

    public function approve()
    {
        $request = Request::validate();

        $response = DB()->update("applications", ["status" => 1], "id = '$request[application]'");

       $applicant = DB()->select("*", "applications as a, users as u, programs as p", "a.user_id = u.id AND a.id = '$request[application]' AND a.program_id = p.id")->get(); 

        $msg_content = "Congratulations! Your application on ".$applicant['program_name']." was approved by the manangement. For more details please go to the SK office. Thank you";


       $email = $this->sendEmail($applicant['email'], $applicant['fullname'], $msg_content);

        echo $email;
    }

    public function sendEmail($email_address, $fullname, $msg_content)
    {
        $subject = "Brgy. Tangub SK Team - Program Application Status";

        $body = "<!DOCTYPE html>
                    <html>
                        <head>
                            <title></title>
                            <link rel='stylesheet' href='".public_url('/assets/adminty/bower_components/bootstrap/css/bootstrap.min.css')."'>
                        </head>
                        <body><h1>Hi ".ucwords($fullname).",</h1>
                    <p>".$msg_content."</p>
                    
                    <p>Best Regards,<br />Management Team</p>
                    Please do not reply.
                        </body>
                    </html>";

        $recepients = $email_address;

        $sent = sendMail($subject, $body, $recepients);

        return $sent;
    }
}
