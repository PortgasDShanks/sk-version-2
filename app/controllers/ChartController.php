<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;


class ChartController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Organizational Chart";

        return view('/chart/index', compact('pageTitle'));
    }


}
