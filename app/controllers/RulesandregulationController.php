<?php

namespace App\Controllers;

use App\Core\App;
use App\Core\Auth;
use App\Core\Request;
use App\Core\Filesystem\Filesystem;


class RulesandregulationController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Rules and Regulation";
        $files = DB()->selectLoop("up.user_id, up.filename, up.created_at, up.id, u.fullname, up.slug, up.filetype, up.iconsize","user_uploads as up, users as u", "up.user_id = u.id AND file_category = 'R' ORDER BY up.created_at DESC")->get();

        return view('/rules/index', compact('pageTitle', 'files'));
    }

    public function storeFile()
    {
        $user_id = Auth::user('id');
        if(Request::hasFile('upload_file')){
            $file_tmp = $_FILES['upload_file']['tmp_name'];
            $file_name = explode('_', str_replace(array('.', ' ', ',', '-'), '_', $_FILES['upload_file']['name']));
            array_pop($file_name);

            $fileSize = $_FILES['upload_file']['size'] / 1000000;
            $file_type = explode('.', $_FILES['upload_file']['name']);
            $file_type_end = end($file_type);
            $fileType = strtoupper($file_type_end);

            $filename = strtoupper(randChar(4)) . date('Ymdhis') . "." . $file_type_end;

            $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
            if (in_array($fileType, $image_icons_array)) {
                $previewSize = '100%';
            } else {
                $previewSize = '25%';
            }

            $folder = uniqid() . '-' . date('Ymdhis');
            $temp_dir = "public/assets/uploads/";

            $folderName = implode('_', $file_name);

            Request::storeAs($file_tmp, $temp_dir, $_FILES['upload_file']['type'], $filename);
            $this->saveFilesInDb($user_id, $temp_dir . $filename, $folderName, $fileSize, $fileType, $previewSize);
            echo $folder;
        }
    }   

    public function saveFilesInDb($user_id, $path, $file_name, $fileSize, $fileType, $previewSize)
    {
      

        $data_form = [
            "user_id" => $user_id,
            "slug" => $path,
            "filetype" => $fileType,
            "filename" => $file_name,
            "filesize" => $fileSize,
            "iconsize" => $previewSize,
            "file_category" => "R"
        ];

        DB()->insert("user_uploads", $data_form);
    }

    public function deleteFile()
    {
        $user_id = Auth::user('id');
        $request = Request::validate();
        $file = new Filesystem;

        if($user_id == $request['user_id']){
            if (Filesystem::exists($request['slug'])) {
                $file->delete($request['slug']);
                $response = DB()->delete("user_uploads", "id = '$request[id]' AND user_id = '$user_id'");
                echo $response;
            }
        }else{
            echo 2;
        }
        
    }

}
