<?php

namespace App\Controllers;

use App\Core\App;
use App\Core\Auth;
use App\Core\Request;
use DateTime;
use DatePeriod;
use DateInterval;


class ReportController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Program Report";

        $program = DB()->selectLoop("*", "programs", "appliable_or_not = 1")->get();

        return view('/reports/index', compact('pageTitle', 'program'));
    }

    public function salesReport()
    {
        $request = Request::validate('', [
            "fromDate" => ['required'],
            "toDate" => ['required'],
            "program" => ['program']
        ]);

        $header['series'] = array();
        $data['name'] = "Total Applicants";
        $data['colorByPoint'] = true;
        $data['data'] = array();

        $begin = new DateTime($request['fromDate']);
        $end = new DateTime($request['toDate']);
        $endmod = $end->modify("+1 day");

        $daterange = new DatePeriod($begin, new DateInterval('P1D'), $endmod);

        foreach($daterange as $date){
            
            $date_value = $date->format("Y-m-d");   
            $list = array();
            $list['name'] = $date->format("M d, Y"); 

            $detail = DB()->select("count(*) as t","applications","DATE_FORMAT(date_applied, '%Y-%m-%d') = '$date_value' AND program_id = '$request[program]' AND status = 1")->get();

            if($detail['t'] == 0 || $detail['t'] == null || $detail['t'] == ''){
                $total = 0;
            }else{	
                $total = $detail['t'] * 1;
            }

            $list['y'] = $total;
            array_push($data['data'], $list);
        }
        array_push($header['series'], $data);
		echo json_encode($header);
    }


}
