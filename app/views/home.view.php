<?php

use App\Core\Auth;
use App\Core\Request;

require 'layouts/head.php'; ?>

<style>
    .welcome-msg {
        margin-top: 10%;
    }
    /* .bg-info {
    background-color: #2dce89 !important;
    } */
    .h7 {
            font-size: 0.8rem;
        }

        .gedf-wrapper {
            margin-top: 0.97rem;
        }

        @media (min-width: 992px) {
            .gedf-main {
                padding-left: 4rem;
                padding-right: 4rem;
            }
            .gedf-card {
                margin-bottom: 2.77rem;
            }
        }

        /**Reset Bootstrap*/
        .dropdown-toggle::after {
            content: none;
            display: none;
        }
        textarea ::placeholder{
            margin-top: 100px;
        }
        .btn_write{
            background-color: #cbcbcb;
            height: 90px;
            border-radius: 111px;
            color: black;
        }
</style>
<div class="header bg-info pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">News Feed</a></li>
            </ol>
            </nav>
        </div>
        </div>
    </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-12 gedf-main">

            <!--- \\\\\\\Post-->
            <div class="card gedf-card">
                <div class="card-header">
                </div>
                <div class="card-body">
                    <div class='row'>
                        <div class='col-sm-1 align-items-center justify-content-center'>
                        <?php if(!empty($user_avatar['user_id'])){?>
                            <img alt="Image placeholder"  src="<?= getImageView($user_avatar['filetype'], $user_avatar['slug']) ?>" class=" rounded-circle" style='height: 65px;width: 65px;margin-top: 30px;'>
                        <?php } else { ?>
                            <img alt="Image placeholder" src="<?= public_url('/storage/images/avatar.png') ?>" class=" rounded-circle" style='height: 65px;width: 65px;margin-top: 30px;'>
                        <?php } ?>
                            
                        </div>
                        <div class='col-sm-11'>
                            
                            <div class="form-group">
                                <label class="sr-only" for="message">post</label>
                                <textarea style='resize:none;border-radius:100px;padding:25px' class='form-control' name="post_text" id="post_text" rows="4"></textarea>
                            </div>
                            <div class="form-group text-right">
                                <button class='btn btn-sm btn-primary' id='saveBtn' onclick='addPost()'><span class='fas fa-plus-circle'></span> Add Post</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Post /////-->

            <!--- \\\\\\\Post-->
            <div id='post-cont'>
            <?php  
            
            if(count($announcements) > 0){ 
                 foreach ($announcements as $announcement) {
                    
                  $getAvatar = DB()->select("*","user_uploads","user_id = '$announcement[user_id]' AND file_category = 'P'")->get();

                  $uploads = DB()->select("*", "user_uploads", " announcement_id = '$announcement[id]'")->get();

                  if(empty($uploads['id'])){
                    $slug = 0;
                    $file = 0;
                  }else{
                    $slug = $uploads['slug'];
                    $file = $uploads['id'];
                  }
            ?>
            <div class="card gedf-card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="mr-2">
                                <?php if(!empty($getAvatar['user_id'])) { ?>
									<img alt="Image placeholder" src="<?= getImageView($getAvatar['filetype'], $getAvatar['slug']) ?>" class="avatar rounded-circle">
								<?php } else { ?> 
									<img alt="Image placeholder" src="<?= public_url('/storage/images/avatar.png') ?>" class="avatar rounded-circle">
								<?php } ?>
                            </div>
                            <div class="ml-2">
                                <div class="h5 m-0"><?=$announcement['fullname']?></div>
                                <div class="h7 text-muted"><?=($announcement['role_id'] == 'C')?"SK Chairman":"SK Member"; ?></div>
                            </div>
                        </div>
                        
                    </div>   
                    <div class='col-sm-12' style='text-align: end;'>
                        
                        <span class=" show-onhover">
                            <i class="fas fa-cog show-onhover text-muted" style="font-size: 14px;cursor: pointer;" onclick="fileOption('<?=$slug?>','<?=$file?>','<?=$announcement['id']?>','<?=$announcement['user_id']?>')"></i>
                        </span>    

                    </div>                  
                </div>
                <div class="card-body">
                    <div class="text-muted h7 mb-2"> <i class="fa fa-clock-o"></i><?=date('M d, Y H:i A', strtotime($announcement['datetime']))?></div>

                    <p class="card-text">
                        <?=$announcement['content']?>
                        <?=getFiles($announcement['id'])?>
                    </p>
                </div>
            </div>  

            <?php } ?>
            </div>
            <?php }else{ ?> 
                <div class="align-items-center justify-content-center" style='text-align: center;'><h2 class="mb-0 text-muted welcome-msg"> No Announcements , Posts As of now... </h2></div>
            
            <?php } ?>
               
            
                                                                                                                       
            </div>
        </div>
    </div>
</div>
<?php include __DIR__ . '/modals/add-post-modal.php'; ?>
<?php include __DIR__ . '/modals/file-options-modal.php'; ?>
<script>
$(document).ready( function(){
    
  
});
function donePosting(){
    $.confirm({
        icon: 'fas fa-check-circle text-green',
        title: 'Success!',
        content: "Your Post / Announcement has been loaded!",
        buttons:{
            Okay: function(){
                $("#add_post_modal").modal('hide');
                $("#post-cont").load(location.href + " #post-cont");
            }
        }
    });
}

function deletePost(){
    var fileID = $("#fileID").val();
    var aID = $("#announcementID").val();
    $.post(base_url+"/delete-post", {
        fileID: fileID,
        aID: aID
    }, function(res){
        success_delete("Post has been successfully deleted!");
    });
}

function downloadFile(){
    var dlFile = $("#slugVal").val();

    window.location.href = base_url+'/'+dlFile;
}

function fileOption(slug, fileID, announcementID, userID){

    var auth = '<?=Auth::user('id')?>';

    (auth == userID) ?$("#deleteDiv").css('display','block'): $("#deleteDiv").css('display','none');
    (fileID != 0) ?$("#fileDiv").css('display','block'): $("#fileDiv").css('display','none');

    $("#fileOptions").modal();
    $("#slugVal").val(slug);
    $("#fileID").val(fileID);
    $("#announcementID").val(announcementID);
}
function showmodal(){
    $('#add_post_modal').modal();
}

// function addPostNoFile(){

// }
function addPost(){
    const message = $("#post_text").val();
    $("#saveBtn").prop("disabled", true);
    $("#saveBtn").html('<span class="fas fa-spin fa-spinner"></span> Posting');
    $.post(base_url+"/add-post", {
        message: message
    }, function(data){
        $.confirm({
          columnClass: 'col-md-6 col-md-offset-3',
          icon: "fas fa-exclamation-triangle text-red",
          title: "CONFIRMATION",
          content: "Before finishing up, Would you like to add Files or Images on this post?",
          buttons: {
            info: {
                text: "No",
                btnClass: 'btn-red',
                action: function(){
                  $.alert("Your Post / Announcement has been loaded!");
                }
            },
            danger: {
                text: "Yes",
                btnClass: 'btn-success any-other-class',
                action: function(){
                  $("#postID").val(data);
                  $("#add_post_modal").modal();

             

                    FilePond.setOptions({
                        server: {
                            url: base_url + "/uploadPostFile/" + data,
                            headers: {
                                'X-CSRF-TOKEN': '<?= Request::csrf_token() ?>'
                            }
                        }
                    });

                    FilePond.registerPlugin(
                        FilePondPluginFileEncode,
                        FilePondPluginFileValidateSize,
                        FilePondPluginImageExifOrientation,
                        FilePondPluginImagePreview
                    );

                    FilePond.create(document.querySelector('input[type="file"]'));
                }
              
            }
          }
      }); 


       
        $("#post_text").val("");
        $("#saveBtn").prop("disabled", false);
        $("#saveBtn").html('<span class="fas fa-plus-circle"></span> Add Post');
    });
}  
</script>
<?php require 'layouts/footer.php'; ?>