<?php 
use App\Core\Auth;
use App\Core\Request;
?>
<div class="modal fade" id="fileOptions" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class='modal-header'>
                <h4> &mdash; File Options &mdash;</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="" id="slugVal">
                <input type="hidden" name="" id="fileID">
                <input type="hidden" name="" id="announcementID">
                <div class="row">
                     <?php if($announcement['user_id'] == Auth::user('id')) { ?>
                    <div class='col-sm-6' id='deleteDiv'>
                        <button onclick='deletePost()' class='btn btn-sm btn-danger btn-round' style='float: right;'><span class='fas fa-trash'></span> Delete Post</button>
                    </div>
                    <?php } ?>
                    <div class='col-sm-6' id='fileDiv'>
                        <button onclick="downloadFile()" class='btn btn-sm btn-primary btn-round'><span class='fas fa-download'></span> Download File</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
</script>