<?php
use App\Core\App;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>
<style>

</style>
<div class="header bg-info pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Operational Manual List</li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="#" onclick="makeModal()" class="btn btn-sm btn-neutral"> Add New</a>
        </div>
        </div>
    </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0"> </h3>
          </div>
          <div class="col-sm-12">
            <div class='table-responsive'>
            <table class="table align-items-center table-flush" id='category_table'>
              <thead class="thead-light">
                <tr style='text-align:center'>
                  <th style='width:128.094px'></th>
                  <th>Filename</th>
                  <th>Added By</th>
                  <th>Created At</th>
                  <th>Action</th>
                </tr> 
              </thead>
              <tbody class="list text-center">
                  <?php
                  $count = 1;
                  foreach ($files as $data_list) {
                    $iconSize = "width: " . $data_list['iconsize'] . ";";
                  ?>
                    <tr style='text-align:center'>
                      <td>
                        <img class="" style='width:25%;' src="<?= getImageView($data_list['filetype'], $data_list['slug']) ?>" alt="Card image cap">
                      </td>
                      <td>
                        <?=$data_list['filename']?>
                      </td>
                      <td><?=$data_list['fullname']?></td>
                      <td><?=date("M d, Y h:i A", strtotime($data_list['created_at']))?></td>
                      <td>
                        <!-- <button onclick='viewFile(<?=$data_list["id"]?>)' class='btn btn-sm btn-info' id='view<?=$data_list['id']?>' data-toggle="tooltip" data-placement="top" title="View File"><span class='fas fa-eye'></span> </button> -->

                        <button onclick='downloadFile("<?=$data_list["slug"]?>")' class='btn btn-sm btn-success' id='download<?=$data_list['id']?>' data-toggle="tooltip" data-placement="top" title="Download File"><span class='fas fa-download'></span> </button>

                        <button onclick='deleteFile(<?=$data_list["user_id"]?>, <?=$data_list["id"]?>, "<?=$data_list["slug"]?>")' class='btn btn-sm btn-danger' id='deleteBtn<?=$data_list['id']?>' data-toggle="tooltip" data-placement="top" title="Delete File"><span class='fas fa-trash'></span> </button>
                      </td>
                    </tr>
                 <?php } ?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include __DIR__ . '/add-file-modal.php'; ?>
<script>
$(document).ready( function(){
  $("#category_table").DataTable();
});
function close_make_modal() {
    location.reload();
}
function makeModal() {
    $('#make_modal').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });
}
function deleteFile(user_id, id, slug){
    $.confirm({
        columnClass: 'col-md-6 col-md-offset-3',
        icon: "fas fa-exclamation-triangle text-red",
        title: "Delete Confirmation",
        content: "Are you sure you want to delete this File ?",
        buttons: {
        info: {
            text: "Cancel",
            btnClass: 'btn-red',
            action: function(){
                $.alert("Action cancelled!");
            }
        },
        danger: {
            text: "Yes, Delete it!",
            btnClass: 'btn-success any-other-class',
            action: function(){
                deleteFiles(user_id, id, slug);
            }
            
        }
        }
    }); 
}
function deleteFiles(user_id, id, slug){
  $.post(base_url + "/operational/delete_file", {
      id: id,
      slug: slug,
      user_id: user_id
  }, function(data) {
    if (data == 1) {
      delete_success("File Successfully Deleted!")
    } else if(data == 2) {
      $.confirm({
          icon: 'fas fa-exclamation-triangle text-orange',
          title: 'Warning!',
          content: "You're not allowed to delete this file!",
          buttons:{
            Okay: function(){
              window.location.reload();
            }
          }
      });
    }else{
        failed_query();
    }
  });
}
function downloadFile(slug){
  window.location.href = base_url + "/" + slug;
}
</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>