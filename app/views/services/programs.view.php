<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>
<style>

</style>
<div class="header bg-info pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Services</a></li>
                <li class="breadcrumb-item active" aria-current="page">Programs</li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="#" data-toggle='modal' data-target='#saveProgram' class="btn btn-sm btn-neutral"> Add New</a>
        </div>
        </div>
        
    </div>
    </div>
</div>
<div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0"> </h3>
            </div>
            <div class="table-responsive">
            <table class="table align-items-center table-flush" id='category_table'>
              <thead class="thead-light">
                <tr style='text-align:center'>
                  <th>#</th>
                  <th>Name</th>
                  <th>Service</th>
                  <th>Description</th>
                  <th>Appliable or Not ?</th>
                  <th>Status</th>
                  <th>Added By</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody class="list">
                  <?php
                  $count = 1;
                  foreach ($programs as $program) {
                    $curdate = date("Y-m-d");
                  ?>
                    <tr style='text-align:center'>
                      <td><?=$count++?></td>
                      <td><?=$program['program_name']?></td>
                      <td><?=$program['service_name']?></td>
                      <td><?=$program['program_desc']?></td>
                      <td><?=($program['appliable_or_not'] == 0)?"<span class='text-red'>NA</span>":"<span class='text-green'>YES</span>"?></td>
                      <td><?=($curdate <= $program['date_to'])?"<span class='text-warning'>ONGOING</span>":"<span class='text-success'>FINISHED</span>"?></td>
                      <td><?=Auth::user('fullname')?></td>
                      <td>
                        <button class='btn btn-sm btn-info' onclick="window.location='<?=route('/services/applicants', $program['program_id'])?>'" id='viewbtn' data-toggle="tooltip" data-placement="top" title="View Applicants"><span class='fas fa-eye'></span></button>
                        
                        <button class='btn btn-sm btn-primary' id='updateBtn' data-toggle="tooltip" data-placement="top" title="Edit Details" onclick='showmodalupdate(<?=$program["program_id"]?>)'><span class='fas fa-edit'></span></button>

                        <!-- <button class='btn btn-sm btn-danger' id='deleteBtn' data-toggle="tooltip" data-placement="top" title="Cancel Program" onclick=''><span class='fas fa-ban'></span></button> -->
                      </td>
                    </tr>
                  <?php } ?>
              </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>
    </div>
    <?php 
      include __DIR__ . '/add-program-modal.php'; 
      include __DIR__ . '/update-program-modal.php';
    ?>
    <script>
      $(document).ready( function(){
        $("#category_table").DataTable();
      });

      $("#savePrograms").on("submit", function(e){
        e.preventDefault();
        const url = base_url+"/services/add-program";
        const data = $(this).serialize();
        $.post(url, data, function(result){
          if(result > 0){
            add_success("Program Successfully Added!");
          }else{
            failed_query();
          }
        });
      });

      $("#updatePrograms").on("submit", function(e){
        e.preventDefault();
        const url = base_url+"/services/update-program";
        const data = $(this).serialize();
        $.post(url, data, function(result){
          if(result > 0){
            add_success("Program Successfully Updated!");
          }else{
            failed_query();
          }
        });
      });

      function showmodalupdate(id){
        $.ajax({
            type: "POST",
            url: base_url + "/services/getprogram-details",
            data: {
              id: id
            },
            dataType: "json",
            success: function (data) {
             $("#program_id").val(id);
             $("#program_name_update").val(data.name);
             $("#program_desc_update").val(data.desc);
             $("#program_date_from_update").val(data.date_from);
             $("#program_date_to_update").val(data.date_to);
             if(data.appliable == 0){
              $("#appliable_check").prop('checked', false);
             }else{
              $("#appliable_check").prop('checked', true);
             }
            }
        })
       $("#updateProgram").modal();
      }
    </script>
    <?php require __DIR__ . '/../layouts/footer.php'; ?>