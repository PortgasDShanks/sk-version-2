<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>
<style>

</style>
<div class="header bg-info pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Programs</a></li>
                <li class="breadcrumb-item active" aria-current="page">Applicants</li>
                <li class="breadcrumb-item active" aria-current="page">
                    <?php
                      $currentdate = date("Y-m-d");
                      $date_to = date("Y-m-d", strtotime($program['date_to']));

                     echo ($currentdate <= $date_to)?"<span class='text-green'>ONGOING</span>":"<span class='text-red'>FINISHED</span>";
                    ?>
                </li>
            </ol>
            </nav>
        </div>
        </div>
        
    </div>
    </div>
</div>
<div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
        <?= alert_msg() ?>
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <div class='row'>
                <div class='col-sm-6'><h2 class="alert alert-info"> <?=$program['program_name']?></h2></div>
                <div class='col-sm-6'><h2 class="alert alert-info"> <?=date("F d, Y", strtotime($program['date_from']))." &mdash; ".date("F d, Y", strtotime($program['date_to']))?></h2></div>
              </div>
            </div>
            <div class="table-responsive">
            <table class="table align-items-center table-flush" id='category_table'>
              <thead class="thead-light">
                <tr style='text-align:center'>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email Address</th>
                  <th>Contact Number</th>
                  <th>Date Applied</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody class="list">
                 <?php $count = 1; foreach ($applicants as $applicant) { ?>
                  <tr style='text-align: center'>
                    <td><?=$count++;?></td>
                    <td><?=$applicant['fullname']?></td>
                    <td><?=$applicant['email']?></td>
                    <td><?=$applicant['contactNo']?></td>
                    <td><?=date("M d, Y", strtotime($applicant['date_applied']))?></td>
                    <td><?=($applicant['status'] == 0)?"<span class='text-orange'>Pending</span>":(($applicant['status'] == 1)?"<span class='text-green'>Approved</span>":"<span  class='text-red'>Cancelled</span>");?></td>
                    <td>
                      <?php if($applicant['status'] == 0) { ?>
                        <button onclick='cancel_application(<?=$applicant["application_id"]?>,<?=$applicant["program_id"]?>)' class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='top' title='Cancel Application' id='cancelBtn'><span class='fas fa-ban'></span></button>

                        <button onclick='approve_application(<?=$applicant["application_id"]?>,<?=$applicant["program_id"]?>)' class='btn btn-sm btn-primary' data-toggle='tooltip' data-placement='top' title='Approve Application' id='approveBtn'><span class='fas fa-thumbs-up'></span></button>
                      <?php } ?>
                    </td>
                  </tr>
                 <?php } ?>
              </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>
    </div>
    <script>
      $(document).ready( function(){
        $("#category_table").DataTable();
      });

      $("#savePrograms").on("submit", function(e){
        e.preventDefault();
        const url = base_url+"/services/add-program";
        const data = $(this).serialize();
        $.post(url, data, function(result){
          if(result > 0){
            add_success("Program Successfully Added!");
          }else{
            failed_query();
          }
        });
      });

      $("#updatePrograms").on("submit", function(e){
        e.preventDefault();
        const url = base_url+"/services/update-program";
        const data = $(this).serialize();
        $.post(url, data, function(result){
          if(result > 0){
            add_success("Program Successfully Updated!");
          }else{
            failed_query();
          }
        });
      });

      function cancel_application(application, program){
        $("#cancelBtn").prop('disabled', true);
        $("#cancelBtn").html("<span class='fas fa-spin fa-spinner'></span>");
        $.post(base_url+"/services/cancel-application", {
          application: application,
          program: program
        }, function(res){
          add_success("Applicants Successfully changed status!");
        });
      }

      function approve_application(application, program){
        $("#approveBtn").prop('disabled', true);
        $("#approveBtn").html("<span class='fas fa-spin fa-spinner'></span>");
        $.post(base_url+"/services/approve-application", {
          application: application,
          program: program
        }, function(res){
          add_success("Applicants Successfully changed status!");
        });
      }
      function showmodalupdate(id){
        $.ajax({
            type: "POST",
            url: base_url + "/services/getprogram-details",
            data: {
              id: id
            },
            dataType: "json",
            success: function (data) {
             $("#program_id").val(id);
             $("#program_name_update").val(data.name);
             $("#program_desc_update").val(data.desc);
             if(data.appliable == 0){
              $("#appliable_check").prop('checked', false);
             }else{
              $("#appliable_check").prop('checked', true);
             }
            }
        })
       $("#updateProgram").modal();
      }
    </script>
    <?php require __DIR__ . '/../layouts/footer.php'; ?>