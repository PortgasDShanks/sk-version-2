<form method="POST" id='savePrograms'>
    <div class="modal fade" id="saveProgram" tabindex="-1" role="dialog" aria-labelledby="saveProgramLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="saveProgramLabel"><span class='fa fa-check-circle'></span> Add Programs</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class='row'>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="program_name">Program Name</label>
                                <input type="text" class="form-control" name="program_name" id="program_name" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="program_desc">Description</label>
                                <textarea name="program_desc" id="program_desc" placeholder='Description' rows="2" class='form-control' style='resize:none'></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="program_date_from">Date From</label>
                                <input class="form-control" name='program_date_from' id='program_date_from' placeholder="" type="date">
                            </div>
                        </div> 
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="program_date_to">Date To</label>
                                <input class="form-control" name='program_date_to' id='program_date_to' placeholder="" type="date">
                            </div>
                        </div> 
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text text-info">Is this Appliable or Not ?</span>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <input type="checkbox" name='appliable_check'>
                                        <input type="hidden" name='service_id' value="<?=$id?>">
                                    </span>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>