<form method="POST" id='saveEvents'>
    <div class="modal fade" id="addEvents" tabindex="-1" role="dialog" aria-labelledby="addEventsLabel" aria-hidden="true" data-backdrop='static'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addEventsLabel"><span class='feather icon-user-plus'></span> Add Doctor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group input-group-merge input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text">Event Name: </span>
                                </div>
                                <input class="form-control" name='eventName' id='eventName' placeholder="" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group input-group-merge input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text">Description: </span>
                                </div>
                                <textarea name="desc" class='form-control' style='resize: none' rows="4"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group input-group-merge input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text">Program: </span>
                                </div>
                                <select name="programID" id="programID" class='form-control'>
                                    <option value="">&mdash; Please Choose &mdash; </option>
                                    <?php foreach ($programs as $program) { ?>
                                        <option value="<?=$program['id']?>"><?=$program['program_name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group input-group-merge input-group-alternative mb-3">
                                <div class="input-group-prepend">
                                <span class="input-group-text">Date: </span>
                                </div>
                                <input type="date" class="form-control" id='eventFrom' name='eventFrom'>
                                <input type="date" class="form-control" id='eventTo' name='eventTo'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary btn-sm">Save changes</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>