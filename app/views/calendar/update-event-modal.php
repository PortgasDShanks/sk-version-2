<form method="POST" id='updateEvents'>
    <div class="modal fade" id="showEvent" tabindex="-1" role="dialog" aria-labelledby="showEventLabel" aria-hidden="true" data-backdrop='static'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="showEventLabel"><span class='feather icon-edit'></span> View / Update Event</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class="form-group">
                                <label for="show_eventName">Name:</label>
                                <input type="text" class="form-control" name="show_eventName" id="show_eventName" >
                                <input type="hidden" name='eventID' id='eventID'>
                            </div>
                        </div>

                        <div class='col-sm-12'>
                            <div class="form-group">
                                <label for="show_desc">Description:</label>
                                <input type="text" class="form-control" name="show_desc" id="show_desc" >
                            </div>
                        </div>
                        
                        <!-- <div class='col-sm-6'>
                            <div class="form-group">
                                <label for="show_date_from">Date From:</label>
                                <input type="date" class="form-control" name="show_date_from" id="show_date_from" >
                            </div>
                        </div>

                        <div class='col-sm-6'>
                            <div class="form-group">
                                <label for="show_date_to">Date To:</label>
                                <input type="date" class="form-control" name="show_date_to" id="show_date_to" >
                            </div>
                        </div> -->
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" id="update_btn" class="btn btn-primary btn-sm">Save changes</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>