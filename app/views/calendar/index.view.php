<?php
use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>
<style>
.fc-button-group {
    background-color: #dddddd;
    border-radius: 5px;
}
</style>
<div class="header bg-info pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Calendar</li>
            </ol>
            </nav>
        </div>
        </div>
    </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0"> </h3>
          </div>
          <div class='card-body'>
            <div class="row">
                <div class='col-sm-12'>
                    <button class='btn btn-sm btn-primary' onclick='showModal()' style='float: right'> <span class='feather icon-plus-circle'></span> Add Event</button>
                </div>
                <div class='col-sm-12 col-xs-12' style='margin-top: 20px;'>
                    <div id='calendar'>
                    
                    </div>
                </div>
                <div class='col-sm-12' id='calendarTrash'>
                    <button style='font-size: 2em;' class='btn btn-sm btn-danger btn-block'> <span class='feather icon-trash-2' style='font-size: font-size: 2em'></span> Event Trash</button>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php require __DIR__ . '/add-event-modal.php'; ?>
    <?php require __DIR__ . '/update-event-modal.php'; ?>
  </div>
    <script>
    function showModal(){
        $("#addEvents").modal({
            show: true
        })
    }

    function deleteEvent(id){
        $.post(base_url+"/calendar/delete-event", {
            id: id
        }, function(res){
            if(res > 0){
                delete_success("Event was successfully deleted!");
            }else{
                failed_query();
            }
        });
    }

    function updateEvent(resized, id){
        $.post(base_url+"/calendar/resize-event", {
            resized: resized,
            id: id
        }, function(res){
            if(res > 0){
                update_success("Event was successfully resized!");
            }else{
                failed_query();
            }
        })
    }

    function dropEvent(new_s, new_e, id){
        $.post(base_url+"/calendar/drop-event", {
            new_s: new_s,
            new_e: new_e,
            id: id
        }, function(res){
            if(res > 0){
                update_success("Event was successfully moved!");
            }else{
                failed_query();
            }
        })
    }
    $(document).ready( function(){
        $("#updateEvents").on('submit', function(e){
            e.preventDefault();
            var url = base_url+"/calendar/update-event";
            var data = $(this).serialize();
            $.post(url, data, function(res){
                $("#showEvent").modal('hide');
                if(res > 0){
                add_success("Event Successfully Updated!");
                }else{
                failed_query();
                }
            });
        });

        $("#saveEvents").on('submit', function(e){
          e.preventDefault();
          var url = base_url+"/calendar/add-event";
          var data = $(this).serialize();
          $.post(url, data, function(res){
            if(res > 0){
              add_success("Event Successfully Added!");
            }else{
              failed_query();
            }
          });
        });

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'year,month,agendaWeek,agendaDay'
            },

            defaultDate: '<?= date('Y-m-d'); ?>',
            editable: true,
            eventLimit: true,
            droppable: true,
            dayRender: function (date, cell) {
             
              var newdate = new Date(date);
              yr      = newdate.getFullYear(),
              month   = newdate.getMonth() < 10 ? '0' + newdate.getMonth() : newdate.getMonth(),
              day     = newdate.getDate()  < 10 ? '0' + newdate.getDate()  : newdate.getDate(),
              newDate = yr + '-' + month + '-' + day;
            
            },
            events:[
            <?php 
                $ctrEvent= 1;
                $user = Auth::user('id');
                $event = DB()->selectLoop("*","events")->get();
                $count = count($event);
                foreach ($event as $events) {
                    $event_title = $events['event_name'];
                    $event_desc = $events['event_description'];
                    $start_date = $events['event_date_from'];
                    $end_date = date("Y-m-d", strtotime('+1 day', strtotime($events['event_date_to'])));
                
            ?>
            {
              title: '<?=$event_title." "."--"." ".$event_desc; ?>',
              titles: '<?=$event_title; ?>',
              start: '<?=$start_date;?>',
              end: '<?=$end_date;?>',
              color: '#3498db',
              id: <?=$events['id']?>,
              description: '<?=$events["event_description"]?>',
              event_date: '<?=date("F d, Y", strtotime($events['event_date_from']))." - ".date("F d,Y", strtotime($events['event_date_to'])); ?>'
          
            }<?php 
              if($ctrEvent < $count){ 
                echo ",";
              }
              $ctrEvent++;
            }
            ?>
          ],
          eventDragStop: function(event,jsEvent) {
              const id = event.id
              const trashEl = jQuery('#calendarTrash');
              const ofs = trashEl.offset();

              const x1 = ofs.left;
              const x2 = ofs.left + trashEl.outerWidth(true);
              const y1 = ofs.top;
              const y2 = ofs.top + trashEl.outerHeight(true);

              if (jsEvent.pageX >= x1 && jsEvent.pageX <= x2 &&
                  jsEvent.pageY >= y1 && jsEvent.pageY <= y2) {
                    $.confirm({
                        icon: 'feather icon-alert-triangle',
                        title: "Confirmation",
                        type: 'orange',
                        content: 'You are about to delete this event.<br> ' +
                                'If you wish to continue, Please press <strong style="font-size: 15px;">Y</strong> to proceed.',
                        buttons: {
                            yes: {
                                isHidden: true,
                                keys: ['y'],
                                action: function () {
                                    deleteEvent(id);
                                }
                            },
                            no: {
                                keys: ['N'],
                                action: function () {
                                    $.alert('Action Cancelled');
                                }
                            },
                        }
                    });
              }
            
          },
          eventResize: function(event , delta , revertfunc){
              const resized_date = event.end.format();
              const event_t = event.title;
              const event_id = event.id;
              $.confirm({
                  icon: 'feather icon-alert-triangle',
                  title: "Confirmation",
                  type: 'orange',
                  content: 'Once you resize the date range, The date of this event will change! .<br> ' +
                          'If you wish to continue, Please press <strong style="font-size: 15px;">Y</strong> to proceed.',
                  buttons: {
                      yes: {
                          isHidden: true,
                          keys: ['y'],
                          action: function () {
                              updateEvent(resized_date, event_id);
                          }
                      },
                      no: {
                          keys: ['N'],
                          action: function () {
                              $.alert('Action Cancelled');
                          }
                      },
                  }
              });
            },
            eventDrop: function(event , delta , revertfunc){
                var title = event.title;
                var change_date = event.start.format();
                var end_change_date = event.end.format();
                var id = event.id;

                $.confirm({
                    icon: 'feather icon-alert-triangle',
                    title: "Confirmation",
                    type: 'orange',
                    content: 'Once you drop it here, The date of this event will change.<br> ' +
                            'If you wish to continue, Please press <strong style="font-size: 15px;">Y</strong> to proceed.',
                    buttons: {
                        yes: {
                            isHidden: true,
                            keys: ['y'],
                            action: function () {
                                dropEvent(change_date,end_change_date,id);
                            }
                        },
                        no: {
                            keys: ['N'],
                            action: function () {
                                $.alert('Action Cancelled');
                            }
                        },
                    }
                });
                
            },
            eventClick: function(callEvent , jsEvent, view){
    
                $("#showEvent").modal();

                $("#show_eventName").val(callEvent.titles);
                $("#show_desc").val(callEvent.description);
                $("#show_date_from").val(callEvent.start);
                $("#show_date_to").val(callEvent.end);
                $("#eventID").val(callEvent.id);

            },
            eventConstraint: {
                start: moment().format('YYYY-MM-DD'),
                end: '2100-01-01'
            }
        });
    });
    </script>
    <?php require __DIR__ . '/../layouts/footer.php'; ?>