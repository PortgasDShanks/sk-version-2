<?php

use App\Core\App;
use App\Core\Auth;
use App\Core\Request;
?>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel='icon' href='<?= public_url('/sk_favicon.ico') ?>' type='image/ico' />
	<title>
		<?= ucfirst($pageTitle) . " | " . App::get('config')['app']['name'] ?>
	</title>

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
	<!-- Icons -->
	<link rel="stylesheet" href="<?= public_url('/assets/argon/vendor/nucleo/css/nucleo.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?= public_url('/assets/argon/vendor/@fortawesome/fontawesome-free/css/all.min.css') ?>" type="text/css">
	<link href="<?= public_url('/assets/argon/fullcalendar/fullcalendar.min.css') ?>"   rel='stylesheet'/>
	<link href="<?= public_url('/assets/argon/fullcalendar/fullcalendar.print.css') ?>" media='print'  rel='stylesheet'/>

	<!-- Argon CSS -->
	<link rel="stylesheet" href="<?= public_url('/assets/argon/css/argon.css?v=1.2.0') ?>" type="text/css">
	<link rel="stylesheet" href="<?= public_url('/assets/argon/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?= public_url('/assets/argon/select2/select2.min.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?= public_url('/assets/argon/sweetalert/sweetalert2.min.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?= public_url('/assets/argon/animate/animate.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?= public_url('/assets/argon/jquery-confirm/jquery-confirm.min.css') ?>" type="text/css">
	<!-- <link rel="stylesheet" href="<?= public_url('/assets/argon/fselect/fSelect.css') ?>" type="text/css"> -->
	

	<style>
		body {
			background-color: #eef1f4;
			font-weight: 350;
		}

		.footer {
			background-color: #eef1f4;
		}

		.bg-primary {
			background-color: #0a9e6e !important;
		}

		.dataTables_wrapper {
			padding: 17px 0px 17px 0px;
		}

		.navbar-brand-img {
			max-height: 3rem !important;
		}
	</style>


	<script src="<?= public_url('/assets/argon/vendor/jquery/dist/jquery.min.js') ?>"></script>
	<!-- Core -->
	<script src="<?= public_url('/assets/argon/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') ?>"></script>
	
	<script src="<?= public_url('/assets/argon/vendor/js-cookie/js.cookie.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/vendor/jquery.scrollbar/jquery.scrollbar.min.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/vendor/datatables.net-bs4/js/jquery.dataTables.min.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/select2/select2.min.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/sweetalert/sweetalert2.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/jquery-confirm/jquery-confirm.min.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/fselect/fSelect.js') ?>"></script>
		
	<!-- Optional JS -->
	<!-- <script src="<?= public_url('/assets/argon/vendor/chart.js/dist/Chart.min.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/vendor/chart.js/dist/Chart.extension.js') ?>"></script> -->
	<script src="<?= public_url('/assets/argon/highcharts/highcharts1.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/highcharts/highcharts-more.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/highcharts/highcharts-drilldown.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/highcharts/exporting.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/fullcalendar/moment.min.js') ?>"></script>
	<script src="<?= public_url('/assets/argon/fullcalendar/fullcalendar.min.js') ?>"></script>

	<?php require_once __DIR__ . '/filepond.php'; ?>
	<script>
		const base_url = "<?= App::get('base_url') ?>";
	</script>
	<?php
	// this will auto include filepond css/js when adding filepond in public/assets
	if (file_exists('public/assets/filepond')) {
		require_once 'public/assets/filepond/filepond.php';
	}
	?>

</head>

<body>

	<!-- Sidenav -->
	<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
		<div class="scrollbar-inner">
			<!-- Brand -->
			<div class="sidenav-header  align-items-center">
				<a class="navbar-brand" href="javascript:void(0)">
				<img src="<?= public_url('/storage/images/sk-logo.png') ?>" alt="SK-logo" style="width: 40px; height: 65px;">
					<?= App::get('config')['app']['name'] ?>
				</a>
			</div>
			<div class="navbar-inner">
				<!-- Collapse -->
				<div class="collapse navbar-collapse" id="sidenav-collapse-main">
					<!-- Nav items -->
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link" href="<?= route('/home') ?>">
								<i class="fas fa-newspaper text-primary"></i>
								<span class="nav-link-text" style='font-weight: 600'>News Feed</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?= route('/calendar') ?>">
								<i class="fas fa-calendar text-primary"></i>
								<span class="nav-link-text" style='font-weight: 600'>Calendar</span>
							</a>
						</li>
						<?php if (getRole(Auth::user('role_id')) == 'C') { ?>
						<li class="nav-item">
							<a class="nav-link" href="<?= route('/members') ?>">
								<i class="fas fa-users text-primary"></i>
								<span class="nav-link-text" style='font-weight: 600'>SK Members</span>
							</a>
						</li>
						<?php } ?>
						<!-- <li class="nav-item">
							<a class="nav-link" href="<?= route('/chart') ?>">
								<i class="fas fa-sitemap text-primary"></i>
								<span class="nav-link-text" style='font-weight: 600'>Organizational Chart</span>
							</a>
						</li> -->
						<li class="nav-item">
							<a class="nav-link" href="<?= route('/services') ?>">
								<i class="fas fa-th-list text-primary"></i>
								<span class="nav-link-text" style='font-weight: 600'>Services</span>
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link" href="<?= route('/reports') ?>">
								<i class="fas fa-th-list text-primary"></i>
								<span class="nav-link-text" style='font-weight: 600'>Reports</span>
							</a>
						</li>
						<!-- <li class="nav-item">
							<a class="nav-link" href="<?= route('/applications') ?>">
								<i class="fas fa-th-list text-info"></i>
								<span class="nav-link-text">Applications</span>
							</a>
						</li> -->
						
					</ul>
				</div>
			</div>
		</div>
	</nav>

	<!-- Main content -->
	<div class="main-content" id="panel">
		<!-- Topnav -->
		<nav class="navbar navbar-top navbar-expand navbar-dark bg-info border-bottom" style="position: sticky;top: 0;z-index: 100;">
			<div class="container-fluid">
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<!-- Navbar links -->
					<ul class="navbar-nav align-items-center  ml-md-auto ">
						<li class="nav-item d-xl-none">
							<!-- Sidenav toggler -->
							<div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
								<div class="sidenav-toggler-inner">
									<i class="sidenav-toggler-line"></i>
									<i class="sidenav-toggler-line"></i>
									<i class="sidenav-toggler-line"></i>
								</div>
							</div>
						</li>
					</ul>
					<ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
						<li class="nav-item dropdown">
						<!-- <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="ni ni-bell-55 text-default"></i>
						</a> -->
						<div class="dropdown-menu dropdown-menu-xl dropdown-menu-right py-0 overflow-hidden">
							<!-- Dropdown header -->
							<div class="px-3 py-3">
							<h6 class="text-sm text-muted m-0">You have <strong class="text-primary">13</strong> notifications.</h6>
							</div>
							<!-- List group -->
							<div class="list-group list-group-flush">
							<a href="#!" class="list-group-item list-group-item-action">
								<div class="row align-items-center">
								<div class="col-auto">
									<!-- Avatar -->
									<img alt="Image placeholder" src="../../assets/img/theme/team-1.jpg" class="avatar rounded-circle">
								</div>
								<div class="col ml--2">
									<div class="d-flex justify-content-between align-items-center">
									<div>
										<h4 class="mb-0 text-sm">John Snow</h4>
									</div>
									<div class="text-right text-muted">
										<small>2 hrs ago</small>
									</div>
									</div>
									<p class="text-sm mb-0">Let's meet at Starbucks at 11:30. Wdyt?</p>
								</div>
								</div>
							</a>
							<a href="#!" class="list-group-item list-group-item-action">
								<div class="row align-items-center">
								<div class="col-auto">
									<!-- Avatar -->
									<img alt="Image placeholder" src="../../assets/img/theme/team-2.jpg" class="avatar rounded-circle">
								</div>
								<div class="col ml--2">
									<div class="d-flex justify-content-between align-items-center">
									<div>
										<h4 class="mb-0 text-sm">John Snow</h4>
									</div>
									<div class="text-right text-muted">
										<small>3 hrs ago</small>
									</div>
									</div>
									<p class="text-sm mb-0">A new issue has been reported for Argon.</p>
								</div>
								</div>
							</a>
							<a href="#!" class="list-group-item list-group-item-action">
								<div class="row align-items-center">
								<div class="col-auto">
									<!-- Avatar -->
									<img alt="Image placeholder" src="../../assets/img/theme/team-3.jpg" class="avatar rounded-circle">
								</div>
								<div class="col ml--2">
									<div class="d-flex justify-content-between align-items-center">
									<div>
										<h4 class="mb-0 text-sm">John Snow</h4>
									</div>
									<div class="text-right text-muted">
										<small>5 hrs ago</small>
									</div>
									</div>
									<p class="text-sm mb-0">Your posts have been liked a lot.</p>
								</div>
								</div>
							</a>
							<a href="#!" class="list-group-item list-group-item-action">
								<div class="row align-items-center">
								<div class="col-auto">
									<!-- Avatar -->
									<img alt="Image placeholder" src="../../assets/img/theme/team-4.jpg" class="avatar rounded-circle">
								</div>
								<div class="col ml--2">
									<div class="d-flex justify-content-between align-items-center">
									<div>
										<h4 class="mb-0 text-sm">John Snow</h4>
									</div>
									<div class="text-right text-muted">
										<small>2 hrs ago</small>
									</div>
									</div>
									<p class="text-sm mb-0">Let's meet at Starbucks at 11:30. Wdyt?</p>
								</div>
								</div>
							</a>
							<a href="#!" class="list-group-item list-group-item-action">
								<div class="row align-items-center">
								<div class="col-auto">
									<!-- Avatar -->
									<img alt="Image placeholder" src="../../assets/img/theme/team-5.jpg" class="avatar rounded-circle">
								</div>
								<div class="col ml--2">
									<div class="d-flex justify-content-between align-items-center">
									<div>
										<h4 class="mb-0 text-sm">John Snow</h4>
									</div>
									<div class="text-right text-muted">
										<small>3 hrs ago</small>
									</div>
									</div>
									<p class="text-sm mb-0">A new issue has been reported for Argon.</p>
								</div>
								</div>
							</a>
							</div>
							<!-- View all -->
							<a href="#!" class="dropdown-item text-center text-primary font-weight-bold py-3">View all</a>
						</div>
						</li>
						<li class="nav-item dropdown">
						<a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-comments text-default"></i><span class="badge badge-pill badge-danger"><?//=messagesCounter()?></span>
						</a>
						<div class="dropdown-menu dropdown-menu-xl dropdown-menu-right py-0 overflow-hidden">
							<!-- Dropdown header -->
							<div class="px-3 py-3">
							<h6 class="text-sm text-muted m-0">You have <strong class="text-primary"><?//=messagesCounter()?></strong> New Message/s.</h6>
							</div>
							<!-- List group -->
							<div class="list-group list-group-flush">
							<?=getNewMessagesNotif()?>
							</div>
							<!-- View all -->
							<a href="<?=route("/chats/0")?>" class="dropdown-item text-center text-primary font-weight-bold py-3">View all Messages</a>
						</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<div class="media align-items-center">
								<?php if(empty(getAvatar())) { ?>
									<img alt="Image placeholder" src="<?= public_url('/storage/images/avatar.png') ?>" class="avatar rounded-circle">
								<?php } else { ?> 
									<img alt="Image placeholder" src="<?= getImageView(getFileType(), getAvatar()) ?>" class="avatar rounded-circle">
								<?php } ?>
									
									<div class="media-body  ml-2  d-none d-lg-block">
										<span class="mb-0 text-sm  font-weight-bold text-default"><?= Auth::user('fullname') ?> 
									</div>
								</div>
							</a>
							<div class="dropdown-menu  dropdown-menu-right ">
								<div class="dropdown-header noti-title">
									<h6 class="text-overflow m-0">Welcome!</h6>
								</div>
								<a href="<?= route('/profile') ?>" class="dropdown-item">
									<i class="ni ni-single-02"></i>
									<span>My profile</span>
								</a>
								<div class="dropdown-divider"></div>
								<a href="<?= route('/rules') ?>" class="dropdown-item">
									<i class="ni ni-bullet-list-67"></i>
									<span>Rules and Regulations</span>
								</a>
								<div class="dropdown-divider"></div>
								<a href="<?= route('/operational') ?>" class="dropdown-item">
									<i class="ni ni-bullet-list-67"></i>
									<span>Operational Manual</span>
								</a>
								<div class="dropdown-divider"></div>
								<a href="<?= route('/logout') ?>" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<i class="ni ni-user-run"></i>
									<span>Logout</span>
								</a>

								<form id="logout-form" action="<?= route('/logout') ?>" method="POST" style="display:none;">
									<?= csrf() ?>
								</form>
							</div>
						</li>
						
					</ul>
				</div>
			</div>
		</nav>

		<!-- Header -->
		<!-- <div class="header pb-6" style="z-index: -1;background-color: #eef1f4;">
			<div class="container-fluid">
				<div class="header-body">
					<div class="row align-items-center py-4">
						<div class="col-lg-6 col-7">
							<h6 class="h2 d-inline-block mb-0"><?= $pageTitle ?></h6>
						</div>
					</div>
				</div>
			</div>
		</div> -->

		<!-- Page content -->
		<!-- <div class="container-fluid mt--6"> -->