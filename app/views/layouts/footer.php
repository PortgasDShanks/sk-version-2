
</div>
<!-- <footer class="footer pt-0">
<div class="row align-items-center justify-content-lg-between">
    <div class="col-lg-6">
    <div class="copyright text-center  text-lg-left  text-muted">
        © 2020 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
    </div>
    </div>
    <div class="col-lg-6">
    <ul class="nav nav-footer justify-content-center justify-content-lg-end">
        <li class="nav-item">
        <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
        </li>
        <li class="nav-item">
        <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
        </li>
        <li class="nav-item">
        <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
        </li>
        <li class="nav-item">
        <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
        </li>
    </ul>
    </div>
</div>
</footer> -->
<script>
$(document).ready( function(){
    $('[data-toggle="tooltip"]').tooltip();
   
})
function alert_response(head, body, icon) {
    Swal.fire(
        head,
        body,
        icon
    );
}
function alert_response_with_link(head, body, icon, link) {
        Swal.fire(
            head,
            body,
            icon
        ).then((value) => {
            window.location = link;
        });
    }
    function alert_response_reload(head, body, icon, link) {
        Swal.fire(
            head,
            body,
            icon
        ).then((value) => {
            window.location.reload();
        });
    }
    function delete_success(message){
        $.confirm({
            icon: 'fas fa-check-circle text-green',
            title: 'Success!',
            content: message,
            buttons:{
              Okay: function(){
                window.location.reload();
              }
            }
        });
    }
    function update_success(message){
        $.confirm({
            icon: 'fas fa-check-circle text-green',
            title: 'Success!',
            content: message,
            buttons:{
              Okay: function(){
                window.location.reload();
              }
            }
        });
    }
    function add_success(message){
        $.confirm({
            icon: 'fas fa-check-circle text-green',
            title: 'Success!',
            content: message,
            buttons:{
              Okay: function(){
                window.location.reload();
              }
            }
        });
    }
    function failed_query(){
        $.confirm({
            icon: 'fas fa-exclamation-triangle text-red',
            title: 'Error Encountered!',
            content: 'There was an error while saving the data',
            buttons:{
              Okay: function(){
                window.location.reload();
              }
            }
        });
    }
  
</script>
</body>

</html>