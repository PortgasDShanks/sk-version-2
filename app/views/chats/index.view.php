<?php

use App\Core\Request;
use App\Core\Auth;

require __DIR__ . '/../layouts/head.php';
?>
<style>

.cg:hover{
  background-color: #ffffff;
  cursor: pointer;
  border-radius: 10px;
}
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
}


.msg-send{
    /* width: 60%; */
    border-radius: 15px;
    padding: 10px;
    background-color: #eef1f4;
}
.msg-reply{
    /* width: 60%; */
    border-radius: 15px;
    position: unset;
    padding: 10px;
    right: 10px;
    top: 6px;
    margin-bottom: 10px;
}
.media-left {
    padding-right: 0px;
}
.user-box .media-object, .friend-box .media-object {
    height: 45px;
    width: 45px;
    display: inline-block;
    cursor: pointer;
    border-radius: 50%;
}

.bg-primary {
    background-color: #eef1f4 !important;
}

.media{
    padding: 8px;
}
</style>
<div class="header bg-info pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Messages</li>
            </ol>
            </nav>
        </div>
        </div>
    </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-md-4">
            <div class='card' style='background-color: #eef1f4;min-height: 500px;'>
                <div class='card-header' style='background-color: #eef1f4;'>
                    <h3>Team Members</h3>
                </div>
                <div class='card-body' style='max-height: 500px;overflow:auto;'>
                  <ul style='list-style: none;padding: 0px;'>
                  <?php foreach ($users as $user) { 
                    $status_color = ($user['login_status'] == 0)?"#ff0000":(($user['login_status'] == 1)?"#00ff00":"#ffa500");  
                    ?>
                      <li class='active_users' style='cursor:pointer' onclick="selected_member(<?=$user['id']?>)">
                        <div class="media align-items-center">
                            <img alt="Image placeholder" src="<?= public_url('/storage/images/avatar.png') ?>" class="avatar rounded-circle">
                            <div class="media-body  ml-2  d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold text-default"><?=$user['fullname']?></span>
                            </div>
                            <!-- <div style="height: 10px; width: 10px; background-color: <?=$status_color?>; border-radius: 50%;display: inline-grid;"></div> -->
                        </div>
                      </li>
                      <hr>
                    <?php } ?>
                    </ul>
                </div>
                <div class='card-footer' style='background-color: #eef1f4;'>
                  <div class="media align-items-center">
                        <img alt="Image placeholder" src="<?= public_url('/storage/images/avatar.png') ?>" class="avatar rounded-circle">
                        <div class="media-body  ml-2  d-none d-lg-block">
                            <span class="mb-0 text-sm  font-weight-bold text-default"><?=Auth::user('fullname')?></span>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class='col-md-8'>
            <div class="card">
                <div class="card-header bg-primary">
                    <div class="media">
                        <a class="media-left" id='avatar' href="#">
                           
                        </a>
                        <div class="media-body">
                            <div class="txt-white" style="padding: 10px;font-weight: bolder;font-size: 18px;" id='sel_name'>&nbsp;</div>
                        </div>
                        <i class="icon-options-vertical f-24 p-absolute msg-ellipsis hidden-md-down"></i>
                    </div>
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12 messages-content" style='padding-top: 10px;'>
                            <div style='max-height: 500px !important; overflow: auto' id='msg'>
                                
                            </div>
                            <div class="messages-send">
                                <div class="form-group">
                                    <div class="input-group input-group-merge input-group-alternative mb-3">
                                        <input type="hidden" id='receiver_id' value=''>
                                        <textarea class="form-control" name='chatContent' id='chatContent' placeholder="Write your message here..." style='resize:none;padding: 15px;border: 3pxsolid #bec1c5;border-radius: 20px;' rows='3'  onclick='readAllMsg()'></textarea>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" onclick='sendMessage()'><i class="ni ni-send text-black" style='font-size: 27px;cursor: pointer;'></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
    <script>
    $(document).ready( function(){
        var receiver_id = '<?=$id?>';
        setTimeout(scrolling, 10);
        loadChat(receiver_id);
        selected_member(receiver_id);
    });
    $(document).keypress(function(e) {
        if(e.which == 13) {
            sendMsg();
        }
        const receiver_id = $("#receiver_id").val();
        loadChat(receiver_id);
    });

    function loadChat(receiver_id){
        viewMessages(receiver_id);
        setTimeout(scrolling, 500);
    }

    function readAllMsg(){
        var receiver = $("#receiver_id").val();
        $.post(base_url+"/chats/read-all-msg", {
            receiver: receiver
        }, function(res){
            
        });
    }
    function selected_member(id){
        $("#msg").html('<div class="d-flex flex-column align-items-center justify-content-center"><h2 class="mb-0 text-muted welcome-msg"><span class="fas fa-spinner fa-spin"></span> Loading Messages</h2></div>');
        $.ajax({
            type: "POST",
            url: base_url+"/chats/selectedMember",
            data:{
                id:id
            },
            // dataType: "json",
            success: function(data){
                const user = JSON.parse(data);
                const userData = user.data[0];

                $("#sel_name").text(userData.name);
                $("#avatar").html(userData.avatar);
                $("#receiver_id").val(userData.id);
                loadChat(userData.id);
            }
        })
    }
    function viewMessages(receiver_id){
        $.ajax({
            type: "POST",
            url: base_url+"/chats/getAllMessages",
            data:{
                receiver_id: receiver_id
            },
            success: function(data){
                const msg = JSON.parse(data);
                console.log(data)
                $("#msg").html(msg.msg_content);
            }
        })
    }

    function sendMessage(){
        const receiver_id = $("#receiver_id").val();
        const chatContent = $("#chatContent").val();
        $("#send_btn").prop("disabled", true);
        $("#send_btn").html("<span class='fas fa-spin fa-spinner'></span> Sending");
        if(chatContent != ""){
            $.ajax({
                type: "POST",
                url: base_url+"/chats/send_msg",
                data:{
                    receiver_id: receiver_id,
                    chatContent: chatContent
                },
                success: function(data){
                    loadChat(receiver_id);
                }
            });
        }else{
            $.confirm({
                icon: 'fas fa-exclamation-triangle text-warning',
                title: 'Warning!',
                content: 'Please write a message before sending',
                buttons:{
                Okay: function(){}
                }
            });
        }
        setTimeout(scrolling, 10);
        $("#chatContent").val("");
        $("#send_btn").prop("disabled", false);
        $("#send_btn").html('<i class="ni ni-send"></i> Send');
        
    }

    function changeStatus(status){
        $.ajax({
            type: "POST",
            url: base_url+"/chats/status-to",
            data:{
                status: status 
            },
            success: function(data){
                $( "#login_status" ).load(window.location.href + " #login_status" );
            },
            error: function(data){
                alert(data);
            }
        })
    }

    function scrolling(){
        var objDiv = document.getElementById("msg");
        objDiv.scrollTop = objDiv.scrollHeight;
    }
    </script>
    <?php require __DIR__ . '/../layouts/footer.php'; ?>