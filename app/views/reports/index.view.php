<?php

use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>
<style>

</style>
<div class="header bg-info pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Programs Report</li>
            </ol>
            </nav>
        </div>
        </div>
    </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0"> </h3>
          </div>
          <div class='col-sm-12'>
          <div class='row'>
            <div class='col-sm-4'>
                <div class="form-group">
                    <label for="program_name">Date From:</label>
                    <input type="date" class="form-control" name="dateFrom" id="dateFrom" >
                </div>
            </div>
            <div class='col-sm-4'>
                <div class="form-group">
                    <label for="program_name">Date To:</label>
                    <input type="date" class="form-control" name="dateTo" id="dateTo" >
                </div>
            </div>
            <div class='col-sm-4'>
                <div class="form-group">
                    <label for="program_name">Programs:</label>
                    <select name="program" id="program" class='form-control'>
                        <option value="">&mdash; Please Choose &mdash; </option>
                        <?php foreach ($program as $programs) { ?>
                            <option value="<?=$programs['id']?>"><?=$programs['program_name']?></option>
                        <?php } ?>
                    </select>   
                </div>
            </div>
            <div class='col-sm-12'>
                <button class='btn btn-sm btn-primary btn-round' onclick="generateGraph()"><span class='fas fa-refresh'></span>Generate</button>
            </div>
            <div class='col-sm-12'>
                <div id='chart' style='width:100% !important; margin-top: 20px;'>
                        
                </div>
            </div>
          </div>

          </div>
        </div>
      </div>
    </div>
  </div>
<script>
    $(document).ready( function(){
    
    })
function generateGraph(){
    var fromDate = $("#dateFrom").val();
    var toDate = $("#dateTo").val();
    var program = $("#program").val();
    $("#chart").html("<span class='fa fa-spin fa-spinner'></span>").css("text-align", "center").css("font-size", "50px");

    $.ajax({
        type: "POST",
        url: base_url + "/reports/program-report",
        data: {
            fromDate: fromDate,
            toDate: toDate,
            program: program
        },
        dataType: "json",
        success: function (data) {
            console.log(data)
            Highcharts.chart("chart", {
            chart: {
                type: 'column'
            },
            title: {
                useHTML: true,
                text: "Total Number of Applicants in this Program"
            },
            xAxis: {
                type: 'category',
                title: {
                text: "Date"
                }
            },
            yAxis: {
                title: {
                text: 'Number of Applicants'
                }
            },
            plotOptions: {
                column: {
                    dataLabels: {
                    enabled: true
                    },
                    enableMouseTracking: true
                },
                series: {
                    connectNulls: true
                }
            },

            tooltip: {
                headerFormat: '',
                pointFormat: '<span style="color:{point.color}">{point.title} </span> <span style="font-size:11px">{series.title}</span>: <b>{point.y:.0f}</b><br/>'
            },

            series: data['series']
            });
        },
        error: function (data) {
                alert(data);
            }
    })
}

</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>